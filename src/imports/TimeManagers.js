import { TimePersonManager } from './TimePersonManager'
import { timeRange } from './helpers.js'

export class TimeManagers{
	constructor(){
		this.timeManagers = [];
	}

	/*
	* Находим самый ранний общий промежуток с минимальным временем spreadMinutes
	* @param {Number} spreadMinutes - необходимый размер промежутка в минутах 
	* @returns {timeDiff} самый ранний общий промежуток
	*/
	findIntersection(spreadMinutes){
		var listFreeSlots = []

		for(let i=0; i<this.timeManagers.length; i++){
			let freeSlots = this.timeManagers[i].findFreeTime(spreadMinutes);
			listFreeSlots.push(freeSlots)
		}

		if(listFreeSlots.indexOf(false) > -1){
			console.warn('in one of timeManger was not found time range');
			return null;
		}

		console.log('listFreeSlots',listFreeSlots)

		return this.recursionFindIntersection(spreadMinutes, listFreeSlots);
	}

	/*
	* Рекурсивная функиция. Второй аргумент каждый раз укорачивается с помощь
	* shift, если пересечения было не найдено
	* @param {Number} spreadMinutes - необходимый размер промежутка в минутах 
	* @param {Array<Array<timeDiff>>} allFreeSlots -  
	* @returns {timeDiff} самый ранний общий промежуток
	*/
	recursionFindIntersection(spreadMinutes, allFreeSlots){
		var firstFreeTimes = [];
		for(let i=0; i<allFreeSlots.length; i++){
			firstFreeTimes.push(allFreeSlots[i][0]);
		}

		var start = this.findMaxStart(firstFreeTimes);
		var end = this.findMinEnd(firstFreeTimes);
		var currentTimeRange = timeRange(start.startTime, end.endTime);

		if(currentTimeRange.freeMinutes < spreadMinutes){
			let sumLength = allFreeSlots.reduce((a,b) => {
				if(a.length)
					return a.length+b.length;
				else
					return a+b.length;
			})
			if(sumLength == allFreeSlots.length){
				console.warn('all timeManagers has not intersection');
				return null
			}

			var diffs = [];
			for(let i=0; i<firstFreeTimes.length; i++){
				diffs.push({
					index: i,
					...timeRange(firstFreeTimes[i].startTime, start.startTime)
				})
			}
			diffs.sort((a,b) => a.freeMinutes < b.freeMinutes)

			for(let i=0; i<diffs.length; i++){
				if(allFreeSlots[diffs[i].index].length > 1){
					allFreeSlots[diffs[i].index].shift();
					return this.recursionFindIntersection(spreadMinutes, allFreeSlots)
					break;
				}
			}
		}else{
			return currentTimeRange;
		}
	}

	/*
	* Получает на вход список из timeDiff's и возвращает,
	* наибольшее начало среди них.
	* @param {Array<timeDiff>} listFreeSlots
	* @returns {timeDiff} элемент listFreeSlots с наибольшим startTime
	*/
	findMaxStart(listFreeSlots){
		var max = 0, maxI;
		for(var i=0; i<listFreeSlots.length; i++){
			let currentStartDate = new Date(`Fri Feb 01 2000 ${listFreeSlots[i].startTime}`);

			if(currentStartDate > max){
				maxI = i;
				max = currentStartDate;
			}
		}

		return listFreeSlots[maxI]//.startTime;
		return listFreeSlots[maxI].startTime;
	}

	/*
	* Получает на вход список из timeDiff's и возвращает,
	* наименьший конец среди них.
	* @param {Array<timeDiff>} listFreeSlots
	* @returns {timeDiff} элемент listFreeSlots с наименьшим endTime
	*/
	findMinEnd(listFreeSlots){
		var min = +Infinity, minI;
		for(var i=0; i<listFreeSlots.length; i++){
			let currentStartDate = new Date(`Fri Feb 01 2000 ${listFreeSlots[i].endTime}`);

			if(currentStartDate < min){
				minI = i;
				min = currentStartDate;
			}
		}

		return listFreeSlots[minI]//.endTime;
		return listFreeSlots[minI].endTime;
	}

	/*
	* Получает расписание
	* @async
	*/
	fetchAsync(){
	    //в реальном случае метод должен делать запрос на сервер для
		//получения расписания. Здесь это статические данные. Промис
		//для схожести с боевым вариантом
		new Promise(function(resolve) {
			resolve([
					[['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']],
					[['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
					[['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]
				])
		})
		.then((schedule) => {
			this.schedule = schedule;

			this.schedule.forEach((schedulePerson)=>{
				this.timeManagers.push(new TimePersonManager(schedulePerson))
			})
		})
	}
}