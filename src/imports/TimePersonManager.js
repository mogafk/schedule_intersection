import { timeRange } from './helpers.js'

//TODO: изменить метод parseTime с использование timeRange

export class TimePersonManager{
	constructor(schedulePerson){
		// добавления ['9:00', '9:00'] и ['19:00', '19:00']
		// для того, чтобы не свободное время с 9 до первого и с последнего до 19
		// занятого слота-времени
		this.schedule = [
			['9:00', '9:00'],
			...schedulePerson,
			['19:00', '19:00']
		];
		this.scheduleFree = [];

		this.extractFreeTime();
	}

	/*
	* Создает в инстансе класса список scheduleFree 
	* состоящий из свободных промежутков времени.
	*/
	extractFreeTime(){
		for(let i=0; i<this.schedule.length-1; i++){
			let currentSlot = this.schedule[i],
				nextSlot = this.schedule[i+1];

			this.parseTime(currentSlot, nextSlot);
		}
	}


	/*
	* Возращает список свободных промежутков, где время промежутка
	* не менее spreadMinutes
	* @param {Number} spreadMinutes - необходимый размер промежутка в минутах
	* @returns {Array<timeDiff>} 
	*/
	findFreeTime(spreadMinutes){
		var res = []
		for(let i=0; i<this.scheduleFree.length; i++){
			if(this.scheduleFree[i].freeMinutes >= spreadMinutes)
				res.push(this.scheduleFree[i]);
		}

		return res.length ? res : false;
	}

	/*
	* Изменяет свойство инстанса класса schuduleFree
	* @param {Array<String, String>} startSlot - массив вида ['hh:mm', 'hh:mm'],
	*								где первый элемент время начала, второй конца
	* @param {Array<String, String>} endSlot - массив вида ['hh:mm', 'hh:mm'],
	*								где первый элемент время начала, второй конца
	*/
	parseTime(startSlot, endSlot){
		let startFreeTime = new Date(`Fri Feb 01 2000 ${startSlot[1]}`),
			endFreeTime = new Date(`Fri Feb 01 2000 ${endSlot[0]}`);

		if(startFreeTime.toString() != endFreeTime.toString()){
			let freeMinutes = (endFreeTime - startFreeTime)/(60*1000);
			

			this.scheduleFree.push({
				freeMinutes,
				startTime: startSlot[1],
				endTime: endSlot[0]
			});
		}
	}
} 