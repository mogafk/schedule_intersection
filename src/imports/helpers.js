//TODO: переименовать startFreeTime endFreeTime
/**
 * Обьект представляет собой структуру описывающую временной промежуток
 * @typedef {Object} timeDiff
 * @property {String} startTime - начало промежутка.
 * @property {String} endTime - конец промежутка.
 * @property {Number} freeMinutes - время в минутах между startTime и endTime
 */


/*
* Получает на вход две строки в формате hh:mm и возвращает timeDiff
* @param {String} startTime - строка в формате hh:mm
* @param {String} endTime - строка в формате hh:mm
* @returns {timeDiff}
*/
export function timeRange (startTime, endTime) {
	let startFreeTime = new Date(`Fri Feb 01 2000 ${startTime}`),
		endFreeTime = new Date(`Fri Feb 01 2000 ${endTime}`);

		let freeMinutes = (endFreeTime - startFreeTime)/(60*1000);
		
		return {
			freeMinutes, startTime, endTime
		};
}


export function timeDiff(time1, time2){
	let startFreeTime = new Date(`Fri Feb 01 2000 ${startTime}`),
		endFreeTime = new Date(`Fri Feb 01 2000 ${endTime}`);


}