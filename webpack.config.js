const path = require('path');
const webpack = require('webpack');

module.exports = {

  context: path.resolve(__dirname, './src'),

  entry: ['babel-polyfill', './app.js'],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist',
  },
  devServer: {
    contentBase: path.resolve(__dirname, '.'), 
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [{
          loader: 'babel-loader',
          options: { 
          	presets: ['es2015', 'stage-0'],
            plugins: ["transform-async-to-generator"]
          }, 
        }],
        exclude: path.resolve(__dirname,'node_modules'),
      }
    ]
  }
};