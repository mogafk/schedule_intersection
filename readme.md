### Тестовая работа(node_version>7.0.0 required)
------
.pdf с заданиями находится в корне проекта.

Задания:

* https://jsfiddle.net/j0v3kmh2/4/ тут я еще не знал, что делаю тестовую работу. Знакомая в чате просто жаловалась, что долго меняет местами массив, строки, умножает их на 2^i и поэтому заинтересовавшись я решил попробовать решить и использовал underscore. Возможно для тестового задания не стоило использовать библиотек. Дальше их использование меньше.
* http://codepen.io/anon/pen/BpxyyE?editors=0010 в данном случае можно было избавиться от mapOp, но оставил, поскольку именно так начал выполнять задание. Пугает потенциальная проблема с памятью, если числа не будут ограничиваться только 1-9
* http://codepen.io/anon/pen/Xpqbay?editors=1010 какое-то извращенное для меня каррирование. Никаких замечаний.
* 4 задание отдельно
----
##### 4 задание

Вот уж где мне удалось поизвращаться, так это в 4 задании. Поскольку явных указаний к выполнению не было, то я разбил код на два класса TimeManagers и TimePersonManager.
Так же для модульности я использовал вебпак; бабел, для es6 синтаксиса и async-await(возможно зря, потому что всего в одном месте и как-то бессмысленно); покрыл один из модулей unit тестами на jasmine.

Собственно само 4 задание это весь этот репозиторий.

Хотелось сделать что-то класное. Расширяемое. С какой-то идей. А не просто функцию, принимающаю на вход и дающаю на выход. Я надеялся немного порефакторить код или добавить возможность пересечения только между конкретными персонами из пула бесчисленных или функционал назначение встречи с изменением доступных промежутков. Отсюда и пришла мысль покрыть код тестами, но честно говоря я уже устал делать задачу, которая не будет реально делать что-то полезное. 

Приблизительно такая задача была на предыдущей работе. Необходимо было назначать время собеседования, групповые собеседование и все такое. У нас использовался moment-range, но поскольку работа тестовая, то я решил, что было бы интереснее обойтись без moment.

----
##### для собственной памяти приложу листинги кода заданий с 1 по 3

```javascript
//1 задание
function calculate() {
let args = [].__proto__.slice.call(arguments)

return _.chain(args)
.map((arr) => {
return arr.split('')
.reverse()
.map((e, i) => e*Math.pow(2, i))
})
.flatten()
.reduce((a,b) => a+b)
.value()
}

console.log(calculate('101', '10'));//7
console.log(calculate('10', '0'));//2
console.log(calculate('10', '10'));//4
```

```javascript
//2 задание
var mapNumbers = {
one: 1,
two: 2,
three: 3,
four: 4,
five: 5,
six: 6
};
var mapOp = {
times: "*",
dividedBy: "/",
plus: "+",
minus: "-"
};

Object.keys(mapOp).forEach((opName) => {
window[opName] = function (num) {
return {
operator: mapOp[opName],
number: num
}
}
});
Object.keys(mapNumbers).forEach((fnName) => {
window[fnName]= function(operatorData){
let number = mapNumbers[fnName]
if(operatorData !== undefined){
switch(operatorData['operator']){
case '*':
return number*operatorData['number'];
case '/':
return number/operatorData['number'];
case '+':
return number+operatorData['number'];
case '-':
return number-operatorData['number'];
default:
console.error('undefined operator');
} 
}

return number;
}
});
console.log(two(times(six())));
console.log(six(plus(one(minus(one())))));
```

```javascript
//3 задание
const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
const ARGUMENT_NAMES = /([^\s,]+)/g;

Function.prototype.__getParamNames = function() {
var func = this;
var fnStr = func.toString().replace(STRIP_COMMENTS, '');
var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
if(result === null)
result = [];
return result;
}

function add (a,b) {
return a+b;
}
function defaultArguments(fn, argObj){
let functionArgs = fn._functionArgs || fn.__getParamNames();

var carryin = function(){
let carryingArgs =
Array.prototype.slice.call(arguments);

if(functionArgs.length !== carryingArgs.length){
let newArgs = functionArgs.map((nameArg, i) => {
return carryingArgs[i] !== undefined ? carryingArgs[i] : argObj[nameArg]
})

return fn.apply(window, newArgs);
}else{
return fn.apply(window, carryingArgs);
}
}

carryin._functionArgs = functionArgs;
return carryin;
}

var add_ = defaultArguments(add, {b: 9 });
console.log(add_(10)); // returns 19 //+
console.log(add_(10, 7)); // returns 17 //+
console.log(add_()); // returns NaN //+
add_ = defaultArguments(add_, { b: 3, a: 2 });
console.log(add_(10)); // returns 13 now //+
console.log(add_()); // returns 5 //+
add_ = defaultArguments(add_, { c: 3 }); // doesn't do anything, since c isn't an argument
console.log(add_(10)); // returns NaN //+
console.log(add_(10, 10)); // returns 20 //+
```