import * as _ from 'lodash';
import { TimePersonManager } from '../src/imports/TimePersonManager.js';

describe("TimePersonManager", function() {
	describe("Получение списка свободных промежутков", function() {
		it('свободное время персоны С из тз', function(){
			let timeManager = new TimePersonManager([['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]);
			let expectTimeFree = [{startTime: '9:00', endTime:'11:30',freeMinutes:150}, 
								  {startTime:'12:15', endTime:'15:00',freeMinutes:165}, 
								  {startTime:'16:30', endTime:'17:45',freeMinutes:75}]

			let expected = _.isEqual(expectTimeFree, timeManager.scheduleFree)
			expect(expected).toBe(true);
		})

		it('свободное время персоны B из тз', function(){
			let timeManager = new TimePersonManager([['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']]);
			let expectTimeFree = [{startTime: '9:00', endTime:'09:15',freeMinutes:15}, 
								  {startTime:'12:00', endTime:'14:00',freeMinutes:120}, 
								  {startTime:'16:30', endTime:'17:00',freeMinutes:30}, 
								  {startTime:'17:30', endTime:'19:00',freeMinutes:90}]

			let expected = _.isEqual(expectTimeFree, timeManager.scheduleFree)
			expect(expected).toBe(true);
		})

		it('свободное время персоны A из тз', function(){
			let timeManager = new TimePersonManager([['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']]);
			let expectTimeFree = [{startTime:'11:30', endTime:'13:30',freeMinutes:120}, 
								  {startTime:'17:30', endTime:'17:45',freeMinutes:15}]

			let expected = _.isEqual(expectTimeFree, timeManager.scheduleFree)
			expect(expected).toBe(true);
		})
	});

	describe("Получение списка свободных промежутков с минимальным временем в минутах", function() {
		it('Заведомо большой промежуток', function(){
			let timeManager = new TimePersonManager([['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']]);
			let freeTimes = timeManager.findFreeTime(999999);
			expect(freeTimes).toBe(false);
		})

		it('Запрос по времени, в который должны попасть все свободные промежутки', function(){
			let timeManager = new TimePersonManager([['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']]);
			let freeTimes = timeManager.findFreeTime(1);
			expect(freeTimes.length).toEqual(timeManager.scheduleFree.length);
		})

		it('Несколько свободных промежутков', function(){
			let timeManager = new TimePersonManager([['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']]);
			let freeTimes = timeManager.findFreeTime(90);
			expect(freeTimes.length).toEqual(2);
		})
	})

	it("Метод parseTime должен изменять внутренний массив", function() {
		let timeManager = new TimePersonManager([['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']]);
		timeManager.parseTime(['19:00', '20:00'], ['21:00', '22:00']);
		let last = _.last(timeManager.scheduleFree)
		let expected = _.isEqual(last, {startTime:'20:00', endTime:'21:00', freeMinutes:60})
		expect(expected).toBe(true);
	})
})